﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASPMVCGentelella.Models
{
    public class TableModel
    {
        public string Message { get; set; }
        public List<CustomerModel> Customers { get; set; }

        public TableModel()
        {
            Customers = new List<CustomerModel>();
        }
    }
}