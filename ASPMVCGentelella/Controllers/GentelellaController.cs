﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ASPMVCGentelella.Controllers
{
    public class GentelellaController : Controller
    {
        // GET: Gentelella
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Index2()
        {
            return View();
        }

        public ActionResult Index3()
        {
            return View();
        }

        public ActionResult form()
        {
            return View();
        }

        public ActionResult form_advanced()
        {
            return View();
        }

        public ActionResult form_validation()
        {
            return View();
        }

        public ActionResult form_wizards()
        {
            return View();
        }

        public ActionResult form_upload()
        {
            return View();
        }

        public ActionResult form_buttons()
        {
            return View();
        }

        public ActionResult general_elements()
        {
            return View();
        }

        public ActionResult media_gallery()
        {
            return View();
        }
        public ActionResult typography()
        {
            return View();
        }
        public ActionResult icons()
        {
            return View();
        }
        public ActionResult Glyphicons()
        {
            return View();
        }
        public ActionResult Widgets()
        {
            return View();
        }
        public ActionResult Invoice()
        {
            return View();
        }
        public ActionResult Inbox()
        {
            return View();
        }
        public ActionResult calendar()
        {
            return View();
        }

        public ActionResult tables()
        {
            return View();
        }

        public ActionResult tables_dynamic()
        {
            return View();
        }

        public ActionResult chartjs()
        {
            return View();
        }
        public ActionResult chartjs2()
        {
            return View();
        }
        public ActionResult morisjs()
        {
            return View();
        }
        public ActionResult echarts()
        {
            return View();
        }
        public ActionResult other_charts()
        {
            return View();
        }
        public ActionResult fixed_sidebar()
        {
            return View();
        }
        public ActionResult fixed_footer()
        {
            return View();
        }

        // Aditional pages
        public ActionResult e_commerce()
        {
            return View();
        }

        public ActionResult projects()
        {
            return View();
        }

        public ActionResult project_detail()
        {
            return View();
        }

        public ActionResult contacts()
        {
            return View();
        }

        public ActionResult profile()
        {
            return View();
        }

        public ActionResult page_403()
        {
            return View();
        }

        public ActionResult page_404()
        {
            return View();
        }

        public ActionResult page_500()
        {
            return View();
        }

        public ActionResult plain_page()
        {
            return View();
        }

        public ActionResult login()
        {
            return View();
        }

        public ActionResult pricing_tables()
        {
            return View();
        }
      
    }
}