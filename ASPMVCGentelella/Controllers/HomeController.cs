﻿using ASPMVCGentelella.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ASPMVCGentelella.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Table()
        {
            ViewBag.Message = "Your Table page.";

            return View();
        }

        public ActionResult TableModel()
        {
            ViewBag.Message = "Your Table page.";

            var tableModel = new TableModel();
            tableModel.Message = "This came from the modelobject";
            tableModel.Customers.Add(new CustomerModel {
                Id = 1,
                Name = "Tiger Nixon",
                Position = "System Architect",
                Office = "Edinburogh",
                Age = 61,
                StartDate = DateTime.Now,
                Salary = 640000m
            });
            tableModel.Customers.Add(new CustomerModel
            {
                Id = 1,
                Name = "Tiger Nixon",
                Position = "System Architect",
                Office = "Edinburogh",
                Age = 45,
                StartDate = DateTime.Now,
                Salary = 640000m
            });
            tableModel.Customers.Add(new CustomerModel
            {
                Id = 1,
                Name = "Garreth Winter",
                Position = "System Architect",
                Office = "Tokyo",
                Age = 65,
                StartDate = DateTime.Now,
                Salary = 620000m
            });
            tableModel.Customers.Add(new CustomerModel
            {
                Id = 1,
                Name = "Ashton Cox",
                Position = "Junior Developer",
                Office = "San Francisco",
                Age = 12,
                StartDate = DateTime.Now,
                Salary = 300000m
            });
            tableModel.Customers.Add(new CustomerModel
            {
                Id = 1,
                Name = "Cedric Kelly",
                Position = "Senior Javascript Developer",
                Office = "Edinburogh",
                Age = 22,
                StartDate = DateTime.Now,
                Salary = 640000m
            });


            return View(tableModel);
        }
    }
}